import javax.swing.JPanel;
import java.awt.*;

public class GamePanel extends JPanel{
    int FPS = 60;
    long targetTimeInMilli = 1000/FPS;
    long waitTIme;
    int x = 0;
    int y = 0;
    int speed = 1;
    int xDirection =1 ;


    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.fillRect(x,y, 100,100);
    }

    public void gameLoop() throws InterruptedException{
        while(true){
            
            if(x>400){
                xDirection =-1;
            }
            else{
                xDirection = 1;
            }
            
           x=x + speed * xDirection;
            repaint();
            long startTime = System.nanoTime();
            System.out.println("startime: "+startTime);

            long endTime = System.nanoTime();
            System.out.println("endTIme: "+endTime);

            long diff = endTime - startTime;
            System.out.println("diff: "+diff);

            waitTIme = targetTimeInMilli - diff/1000000;
            System.out.println("waitTIme: "+waitTIme);

            Thread.sleep(waitTIme);
        }
    }
}
