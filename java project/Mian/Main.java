import javax.swing.*;

public class Main{
    public static void main(String[] args) throws InterruptedException {
        JFrame frame = new JFrame("DVD");
        frame.setSize(500,500);

        frame.setLocationRelativeTo(null);

        GamePanel gp = new GamePanel();
        frame.add(gp);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.setVisible(true);
        gp.gameLoop();
    }


}