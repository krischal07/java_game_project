import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main{
    public static void main(String[] args) throws IOException 
    {
        // System.out.println("Hello World");

        // Create a new file Hello.txt
        // File file = new File("Hello.txt");
        // try {
        //     file.createNewFile();
        // } catch (IOException e) {
        //     // TODO Auto-generated catch block
        //     e.printStackTrace();
        // }

        // Write in a file Hello.txt
        // try {
        //     FileWriter fw = new FileWriter("Hello.txt");//Pen Concept
        //     fw.write("This is written from code");
        //     fw.close();//Always close the file
        // } catch (IOException e) {
        //     // TODO Auto-generated catch block
        //     e.printStackTrace();
        // } 

        try {
            FileReader fr=new FileReader("Hello.txt");
            int value;

            while((value = fr.read())!= -1){
                System.out.print((char)value);
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}