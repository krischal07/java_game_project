import javax.swing.JFrame;
import javax.swing.*;

public class Main{
    public static void main(String[] args) {
        
        JFrame frame = new JFrame("Box Collision");
        frame.setSize(600,600);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        GamePanel game = new GamePanel();
        frame.add(game);
        
        game.startGame();
        frame.setVisible(true);
    }
}