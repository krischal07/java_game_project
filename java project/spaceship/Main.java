import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
    public static void main(String[] args) {
        // System.out.println("Hello WOrld");
        JFrame frame = new JFrame("Spaceship game");
        frame.setSize(600,600);

        Spaceship sp = new Spaceship();
        sp.setPreferredSize(new Dimension(500,300));
        frame.add(sp,BorderLayout.NORTH);

        
        JPanel actionPanel = new JPanel();

        JButton left = new JButton("Left");
        actionPanel.add(left);
        left.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                System.out.println("Left");
                sp.moveLeft();
            }
        });


        JButton right = new JButton("Right");
        actionPanel.add(right);
        right.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                System.out.println("Right");
                sp.moveRight();
            }
        });
        
        frame.add(actionPanel, BorderLayout.SOUTH);

        frame.setVisible(true);
        frame.setLayout(null);

        

    }
}
