import java.awt.*;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

 class Spaceship extends JPanel{
    private Image spaceShipImage;

    Spaceship(){
        super();
        try {
            spaceShipImage = ImageIO.read(new File("./ship1.png"));
        } catch (IOException e) {
        }
    }

    int x =0;
    int y =0;

    public void moveLeft(){
        x-=10;
        repaint();
    }   

    public void moveRight(){
        x+=10;
        repaint();
    }

        public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.red);
        // g.fillRect(x, y,100, 100);
            g.drawImage(spaceShipImage, x, y, 100,100, this);
    }
}
