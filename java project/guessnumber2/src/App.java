import java.util.Random;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
       Random rand = new Random();
        Scanner scanner = new Scanner(System.in);

       int x = rand.nextInt(10);
        int y = 0;
        int count = 0;

       while(true){
       System.out.println("Enter the number:");
         y = scanner.nextInt();
            count++;
            if(x==y){
                System.out.println("You have guessed the number.\n");
                break;
            }
            else if(x>y){
                System.out.println("Higher");
            }
            else{
                System.out.println("Lower");
            }
        }
        System.out.println("You have guessed in "+ count+ " try.");
    }
}
