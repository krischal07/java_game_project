import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Player extends JPanel{
    int x=460;
    int y=530;
    int speed = 5;
    int directionX = 0;
    int directionY = 0;
    Image image;
    ArrayList<Bullet> bullets = new ArrayList<>();
    // Image shoot;
    // boolean isShooting=false;


    Player() throws IOException{
        image = ImageIO.read(new File("./spaceship2.png"));
        // shoot = ImageIO.read(new File("./fireblast1.png"));
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        // g.setColor(Color.WHITE);
        // g.fillRect(x, y, 100, 100);
        g.drawImage(image, x,y, 65,90, this);

        // if(isShooting==true){ (FOR BULLET)
        // g.drawImage(shoot, 500, 600, 50,50,this);
        // }
        for(Bullet bullet:bullets){
            bullet.paintComponent(g);
        }
    }

    public void playerUpdate(KeyEvent e) throws IOException{
        int code = e.getKeyCode();
        System.out.println(code);
      
        if(code == 37){//left
            directionX = -1;
        }
        else if (code == 39){//right
            directionX = 1;
        }
        else if (code == 38){//up
            directionY = -1;
        }
        else if (code == 40){//down
            directionY = 1;
        }

        // else if(code == 32){
        //     isShooting=true;
        // }

        else if(code == 32){
            try{
            bullets.add(new Bullet(x+20, y));
            System.out.println("Bullet clicked:"+ bullets);
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
    }

    public void move(){
        x = x + directionX * speed;
        y = y + directionY * speed;
        // if(x<0){
        //     x = 0;
        // }
        // else if(x>400){
        //     x = 400;
        // }
        // else if(y>370){
        //     y=370;
        // }
        // else if(y<0){
        //     y =0;
        // }
        // repaint();

        for(int i=0;i<bullets.size();i++){
            Bullet bullet = bullets.get(i);
            bullet.update();
            if(bullet.y<0){
                bullets.remove(i);
                System.out.println("Bullet Removed:" + bullet);
                i--;
            }
        }

    }
    public void stop(){
        directionX = 0;
        directionY = 0;
    }
    public Rectangle getBounds(){
        return new Rectangle(x, y, 65, 90); 
    }

    
}
