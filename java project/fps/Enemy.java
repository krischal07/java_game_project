import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Enemy extends JPanel {
    Random rand = new Random();
    int x = rand.nextInt(750);
    int y=  rand.nextInt(-300,0);
    Image image;
    int directionY = 1;
    int speed = rand.nextInt(3);
    Enemy() throws IOException{
        image = ImageIO.read(new File("./Asteroid.png"));
    }
      public void paintComponent(Graphics g){
        super.paintComponent(g);
        // g.setColor(Color.CYAN);
        // g.fillRect(x,y,100,100);
        g.drawImage(image,x, y, 70, 70,this);
    }


    //Movemet of enemy
    public void update(){
        y+=1;

        if(y>720){
             x = rand.nextInt(700);
             y=  rand.nextInt(-300,0);
            }
        y=y+directionY*speed;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 50, 50); // Size of the player image
    }

}

