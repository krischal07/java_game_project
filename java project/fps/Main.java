
import java.io.IOException;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws IOException {
        
        JFrame frame = new JFrame("FPS control loop");
        frame.setSize(1000,700);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GamePanel game =  new GamePanel();
        frame.add(game);

        game.startGame();
        frame.setVisible(true);
    }
}
