import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;


public class GamePanel extends JPanel implements Runnable {
    Thread thread;
    // Enemy enemy = new Enemy();
    Enemy[] enemies = new Enemy[5];
    Player player = new Player();
    Image image;
    boolean isRunning = true;
    Bullet[] bullet = new Bullet[5];

    GamePanel() throws IOException{
        super();
        image = ImageIO.read(new File("./space-bg-2.jpg"));

        // Set Background color black
        this.setPreferredSize(new Dimension(500,500));
        // this.setBackground(Color.BLACK);
        for(int i=0;i<enemies.length;i++){
            enemies[i] = new Enemy();
            }
            setFocusable(true);
            addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e){
                    //Adding a player Movement
                    // System.out.println("User have pressed a key");
                    try {
                        player.playerUpdate(e);
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    }

                    public void keyReleased(KeyEvent e){
                        player.stop();
                    }

                    });
                    }
                    public void paintComponent(Graphics g){
                        super.paintComponent(g);
                        // Set Image in background of the panel
                        g.drawImage(image,0 ,0,1000,700,null);

                        player.paintComponent(g);
                        
                        //Bring 10 enemies
                        for(int i=0;i<enemies.length;i++){
                            enemies[i].paintComponent(g);
                            
        }
        }

        
    public void startGame(){
        thread = new Thread(this);
        thread.start();
        }
        
        public void update(){
            //Move the enemy from here
            //Bring 10 enemies
            for(int i=0;i<enemies.length;i++){
                enemies[i].update();
                if(player.getBounds().intersects(enemies[i].getBounds())){
                    System.out.println("Collison detected");
                    isRunning = false;
                    // break;
                }
                }

                // for(int i=0;i<bullet.length;i++){
                //     System.out.println("Bullet Updated");
                //     if(bullet[i].getBounds().intersects(enemies[i].getBounds())){
                //         System.out.println("Bullet Intersected");
                //     }
                // }
                //Moving player
                player.move();
            }

    

    @Override
    public void run() {
        double drawInterval = 1000000000/60;
        double deltaTime = 0;
        long lastPassedTime = System.nanoTime();
        long currentTime = 0 ;

        while(thread!=null && isRunning==true){
            
            // long time = System.nanoTime();
            // System.out.println("Game Loop: "+ time);
            // x += 1;

            currentTime = System.nanoTime();
            deltaTime += (currentTime-lastPassedTime)/drawInterval;
            lastPassedTime = currentTime;

            if(deltaTime >= 1){
                //Movement of the box
                // y = y + 1; Remove this from gamepanel
                update();
                repaint();
                deltaTime--;
            }

        }
        // System.out.println("Game loop");
    }
}
