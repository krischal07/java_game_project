import java.awt.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Bullet {
    int x,y;
    int speed = 5;
    Image image;

    Bullet(int a, int b) throws IOException{
        this.x = a;
        this.y = b;
        image = ImageIO.read(new File("./fireblast1.png"));
    }

    public void update(){
        y-=speed;
    }

    public void paintComponent(Graphics g){
        g.drawImage(image, x, y,10,20, null);

}
public Rectangle getBounds() {
    return new Rectangle(x, y, 10, 20); 
}
}
