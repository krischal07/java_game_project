import java.util.Random;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("\nPlease Choose:");
        System.out.println("0. Scissors");
        System.out.println("1. Paper");
        System.out.println("2. Rock");
        
        int x = rand.nextInt(3);

        System.out.println("User, please choose:");
        int y = scanner.nextInt();

        // Print CPU's choice
        switch(x) {
            case 0:
                System.out.println("CPU has Scissors");
                break;
            case 1:
                System.out.println("CPU has Paper");
                break;
            case 2:
                System.out.println("CPU has Rock");
                break;
        }

        // Print User's choice
        switch(y) {
            case 0:
                System.out.println("User has Scissors");
                break;
            case 1:
                System.out.println("User has Paper");
                break;
            case 2:
                System.out.println("User has Rock");
                break;
            default:
                System.out.println("Invalid choice.");
                return;
        }

        if (x == y) {
            System.out.println("It's a tie!");
        } else if ((y == 0 && x == 1) || (y == 1 && x == 2) || (y == 2 && x == 0)) {
            System.out.println("User wins!");
        } else {
            System.out.println("CPU wins!");
        }
    }
}
